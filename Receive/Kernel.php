<?php

namespace Webformat\Http\SignedInteraction\Receive;

abstract class Kernel
{
    protected $request;
    protected $runtime = [];
    protected $response = [
        'status' => '',
        'data' => [],
        'errors' => [],
    ];
    protected $isValid;
    // protected $callbacks = ['before_send' => [], 'about_to_send' => []];

    public function __construct(?Object $request, array $runtimeOpts = [])
    {
        $this->request = $request;
        $this->runtime = $runtimeOpts;

        if($this->isValid = $this->requestIsValid()){
            $this->isValid = $this->init();
        }
    }

    public function requestIsValid(): bool
    {
        if(\is_bool($this->isValid)){
            return $this->isValid;
        }
        if(empty($this->request)){
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Empty request';

            return false;
        }

        return true;
    }

    abstract public function init(): bool;

    public function getResponse(): array
    {
        return $this->response;
    }

    public function getRequest()
    {
        return $this->request;
    }

    abstract public function getRequestString(string $name): ?string;
    abstract public function getRequestAttachments(): array;
    abstract public function attachIsValid(&$requestAttach, string $hashExpected): bool;

    abstract public function getKey2Dec(): ?string;
    abstract public function getSalt(): ?string;
}
