<?php

namespace Webformat\Http\SignedInteraction\Receive;

use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;

class Director
{
    protected $kernel;
    protected $response = [
        'status' => '',
        'data' => [],
        'errors' => [],
    ];

    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
    }

    public function process()
    {
        if (!$this->kernel->requestIsValid()) {
            return $this->kernel->getResponse();
        }

        $payload = $this->getPayloadDecrypted();
        if (\is_null($payload)) {
            return $this->response;
        }

        $approvedAttachments = [];

        if (!empty($payload['attachments'])) {
            $rawAttachments = $this->kernel->getRequestAttachments();

            $approvedAttachments = $this->getApprovedAttachments($rawAttachments, $payload['attachments']);
            if (\count($rawAttachments) != \count($approvedAttachments)) {
                $response = $this->kernel->getResponse();
                $response['status'] = 'error';
                $response['errors'][] = 'Untrusted attachments';

                return $response;
            }
        }

        $delegateResult = $this->delegate($payload, $approvedAttachments);

        if (\is_null($delegateResult)) {
            $this->response['status'] = 'error';

            return $this->response;
        }

        $response = $this->kernel->getResponse();
        $response['status'] = 'ok';
        $response['data'] = $delegateResult;

        return $response;
    }

    protected function getPayloadDecrypted(): ?array
    {
        if (!$payload = $this->kernel->getRequestString('payload')) {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Empty payload';

            return null;
        }

        $ecnryptedChunks = \array_map('base64_decode', \explode('|', $payload));

        // if (!$payload = \base64_decode($this->kernel->getRequestString('payload'))) {
        if (!$ecnryptedChunks) {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Payload syntax error';

            return null;
        }
        $payloadDecrypted = '';
        $stringifiedBy = $this->kernel->getRequestString('stringified_by');

        if ($key2dec = $this->kernel->getKey2Dec()) {
            foreach ($ecnryptedChunks as $encryptedChunk) {
                $plainChunk = '';
                if (\openssl_public_decrypt($encryptedChunk, $plainChunk, $key2dec)) {
                    $payloadDecrypted .= $plainChunk;
                } else {
                    $this->response['status'] = 'error';
                    $this->response['errors'][] = 'Payload source isn\'t trusted';

                    return null;
                }
            }
            $payload = ('json' == $stringifiedBy) ? \json_decode($payloadDecrypted, true) : \unserialize($payloadDecrypted);
        } else {
            $payload = \implode('', $ecnryptedChunks);
            $algo = $this->kernel->getRequestString('algo');
            $sign = $this->kernel->getRequestString('sign');
            if (empty($algo) || empty($sign)) {
                $this->response['status'] = 'error';
                $this->response['errors'][] = 'Payload isn\'t signed';

                return null;
            }

            $salt = $this->kernel->getSalt();
            if (empty($salt)) {
                $this->response['status'] = 'error';
                $this->response['errors'][] = 'The crypto salt shouldn\'t be empty!';

                return null;
            }

            if (!(string) ($hash = \hash($algo, $payload.$salt))) {
                $this->response['status'] = 'error';
                $this->response['errors'][] = 'Hash calculating error (unexpected algo?)';

                return null;
            }

            if ($hash !== $sign) {
                $this->response['status'] = 'error';
                $this->response['errors'][] = 'Payload sign mismatch';

                return null;
            }
            $payload = ('json' == $stringifiedBy) ? \json_decode($payload, true) : \unserialize($payload);
        }

        return $payload;
    }

    protected function getApprovedAttachments(array $rawAttachments, array $hashes): array
    {
        $approved = [];
        if (!$rawAttachments || !$hashes) {
            return [];
        }

        foreach ($hashes as $index => $hashExpected) {
            if (!isset($rawAttachments[$index])) {
                continue;
            }
            $candidate = $rawAttachments[$index];
            if ($this->kernel->attachIsValid($candidate, $hashExpected)) {
                $approved[$index] = $candidate;
            }
        }

        return $approved;
    }

    protected function delegate(array $payload, array $attachments): ?array
    {
        if (!($commandClassname = $payload['cmd'] ?: '')) {
            $this->response['errors'][] = 'Empty command';

            return null;
        }

        if (!\class_exists($commandClassname)) {
            $this->response['errors'][] = 'Unexpected command';

            return null;
        }

        if (!\is_subclass_of($commandClassname, BaseCommand::class)) {
            $this->response['errors'][] = 'Unexpected command classname';

            return null;
        }
        $command = new $commandClassname($this->kernel->getRequest());
        $command->setAttachments($attachments);
        try {
            $params = $payload['params'] ?: [];
            $results = $command(...\array_values($params));
        } catch (\Throwable $er) {
            if (!$errors = \json_decode($er->getMessage(), true)) {
                $errors = [$er->getMessage()];
            }
            $this->response['errors'] = \array_merge($this->response['errors'], $errors);

            return null;
        }

        return $results;
    }
}
