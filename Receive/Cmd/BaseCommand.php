<?php
namespace Webformat\Http\SignedInteraction\Receive\Cmd;

abstract class BaseCommand
{
    protected $request;
    protected $attachments = [];
    protected $response = [
        'status' => '',
        'data' => [],
        'errors' => [],
    ];

    public function __construct(Object $request)
    {
        $this->request = $request;
    }

    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    abstract public function __invoke(...$params): array;
}
