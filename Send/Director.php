<?php

namespace Webformat\Http\SignedInteraction\Send;

class Director
{
    protected $kernel;
    protected $encryptChunkSize = 100; //bytes

    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
    }

    public function send(array $payload, array $attachments = [], array $addition = []): ?string
    {
        $eventParams = [&$request];
        $this->kernel->fire('before_send', $eventParams);
        $endpoint = $this->kernel->getEndpoint();

        $payload['attachments'] = [];
        foreach ($attachments as $index => $finfo) {
            $payload['attachments'][$index] = \md5_file($finfo['path']);
        }

        $payload = \serialize($payload);
        $payloadEncrypted = $this->encrypt($payload);
        $request = [
            'payload' => $payloadEncrypted ?: \base64_encode($payload),
        ];
        if (!$payloadEncrypted) {
            // we should at least sign our payload by the hash with salt
            if ($signInfo = $this->getHash($payload)) {
                $request += $signInfo;
            } else {
                throw new \Exception('Can\'t sign a request');
            }
        }
        $responseCode = 0;
        $request += $addition;
        $this->kernel->fire('about_to_send', $eventParams);
        $response = $this->kernel->post($endpoint, $request, $attachments, $responseCode);

        if (200 != $responseCode) {
            throw new \Exception('Wrong response http status code '.(string) $responseCode);
        }

        return $response;
    }

    protected function encrypt(string $data): ?string
    {
        if (
            (!$key2enc = $this->kernel->getKey2Enc()) ||
            !\function_exists('openssl_private_encrypt')
        ) {
            return null;
        }
        $encryptedChunks = [];
        $chunks = \mb_str_split($data, $this->encryptChunkSize, '8bit');

        foreach($chunks as $plainChunk){
            $encryptedChunk = '';
            if (!\openssl_private_encrypt($plainChunk, $encryptedChunk, $key2enc)) {
                return null;
            }
            $encryptedChunks[] = \base64_encode($encryptedChunk);
        }

        return \implode('|', $encryptedChunks);
    }

    protected function getHash(string $data): ?array
    {
        $salt = $this->kernel->getSalt();
        if (empty($salt)) {
            return null;
        }
        if (!$sha512Algos = \preg_grep('/sha.*512/i', \hash_algos())) {
            return null;
        }
        $algo = \preg_grep('/sha3/', $sha512Algos) ?: \preg_grep('/^sha512$/i', $sha512Algos) ?: $sha512Algos;
        $algo = \current($algo);
        if (!(string) ($hash = \hash($algo, $data.$salt))) {
            return null;
        }

        return [
            'algo' => $algo,
            'sign' => $hash,
        ];
    }
}
