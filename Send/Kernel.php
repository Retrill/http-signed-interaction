<?php

namespace Webformat\Http\SignedInteraction\Send;

abstract class Kernel
{
    protected $runtime = [];
    protected $callbacks = ['before_send' => [], 'about_to_send' => []];

    public function __construct(array $runtimeOpts)
    {
        $this->runtime = $runtimeOpts;
    }

    abstract public function getEndpoint(): string;

    public function addCallback(string $eventName, callable $callback)
    {
        if (!\array_key_exists($eventName, $this->callbacks)) {
            throw new \Exception('Unexpected event name "'.$eventName.'"!');
        }
        $this->callbacks[$eventName][] = $callback;
    }

    public function fire(string $eventName, array &$callbackParams)
    {
        if (!empty($this->callbacks[$eventName])) {
            foreach ($this->callbacks[$eventName] as $callback) {
                \call_user_func_array($callback, $callbackParams);
            }
        }
    }

    abstract public function getKey2Enc(): ?string;

    abstract public function getSalt(): ?string;

    abstract public function post(string $url, array $data, array $attachments, &$responseCode): ?string;
}
